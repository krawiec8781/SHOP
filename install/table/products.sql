--liquibase formatted sql
--changeset kubaf:brands failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table brands; commit;

create table products(
    id number generated always as identity (start with 1 increment by 1) primary key,
    brand_id number not null,
    product_type_id number not null,
    model_name varchar2(200) not null,
    price number not null
)