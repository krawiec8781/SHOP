--liquibase formatted sql
--changeset kubaf:employees failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table employees; commit;

create table employees(
    id  number generated always as identity (start with 1 increment by 1)  primary key,
    name varchar(200) not null,
    surname varchar(200) not null,
    position varchar(200) not null
)
