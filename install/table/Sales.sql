--liquibase formatted sql
--changeset pkrawczyk:Sales failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table Sales;

create table Sales
(
    id               number generated always as identity (start with 1 increment by 1) primary key,
    product_id       number not null,
    sale_date        date not null,
    employee_id      number not null,
    peroid_event_id number default null,
    Calculated      char default 'N' not null
)
;
