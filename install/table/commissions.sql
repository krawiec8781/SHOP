--liquibase formatted sql
--changeset pkrawczyk:commissions failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table commissions;

create table commissions
(
  id                    number generated always as identity (start with 1 increment by 1)  primary key,
  employee_id number not null,
  month number not null,
  year number not null,
  total_commission number
)
;
