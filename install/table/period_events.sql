--liquibase formatted sql
--changeset pkrawczyk:period_events failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table period_events;

create table period_events
(
    id         number generated always as identity (start with 1 increment by 1) primary key,
    name       varchar2(200) not null,
    start_date date          not null,
    end_date   date          not null,
    commission number        not null
)
;
