--liquibase formatted sql
--changeset pkrawczyk:products_types failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table products_types;

create table products_types
(
    id         number generated always as identity (start with 1 increment by 1) primary key,
    name       varchar2(200) not null,
    commission number not null

)
;
