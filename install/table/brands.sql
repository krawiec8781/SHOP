--liquibase formatted sql
--changeset kubaf:brands failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback drop table brands; commit;

create table brands(
    id number generated always as identity (start with 1 increment by 1) primary key,
    name varchar(200) not null,
    commission number not null
)