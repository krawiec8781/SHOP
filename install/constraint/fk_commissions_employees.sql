--liquibase formatted sql
--changeset kubaf:fk_commissions_employees failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table commissions drop constraint fk_commissions_employees;

alter table commissions
  add constraint fk_commissions_employees
    foreign key (employee_id)
      references employees (id)
--on delete cascade
;