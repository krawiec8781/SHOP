--liquibase formatted sql
--changeset pkrawczyk:fk_sales_products failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table sales drop constraint fk_sales_products;

alter table sales
  add constraint fk_sales_products
    foreign key (PRODUCT_ID)
      references PRODUCTS (id)
--on delete cascade
;