--liquibase formatted sql
--changeset kubaf:fk_sales_employees failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table sales drop constraint fk_sales_employees;

alter table sales
  add constraint fk_sales_employees
    foreign key (employee_id)
      references employees (id)
--on delete cascade
;
