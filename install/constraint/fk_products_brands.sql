--liquibase formatted sql
--changeset pkrawczyk:fk_products_brands failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table products drop constraint fk_products_brands;

alter table PRODUCTS
  add constraint fk_products_brands
    foreign key (BRAND_ID)
      references BRANDS (id)
--on delete cascade
;