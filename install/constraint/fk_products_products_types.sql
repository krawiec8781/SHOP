--liquibase formatted sql
--changeset pkrawczyk:fk_products_products_types failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table products drop constraint fk_products_products_types;

alter table products
  add constraint fk_products_products_types
    foreign key (PRODUCT_TYPE_ID)
      references PRODUCTS_TYPES (id)
--on delete cascade
;