--liquibase formatted sql
--changeset kubaf:fk_sales_period_events failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback alter table sales drop constraint fk_sales_period_events;

alter table sales
  add constraint fk_sales_period_events
    foreign key (PEROID_EVENTS_ID)
      references PERIOD_EVENTS (id)
--on delete cascade
;

