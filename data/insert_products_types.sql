--liquibase formatted sql
--changeset pkrawczyk:insert_products_types failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback delete from products_types where 1=1 ; commit;

INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES ('Laptop', 10);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('TV', 8);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('Telefon', 9);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('Smartwatch', 7);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('Sluchawki', 10);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('Glosniki', 8);
INSERT INTO PRODUCTS_TYPES (NAME, COMMISSION) VALUES('Drukarka', 7);

COMMIT ;