--liquibase formatted sql
--changeset pkrawczyk:insert_brands failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback delete from brands where 1=1 ; commit;

insert into BRANDS (name, COMMISSION) values ('Sony', 4);
insert into BRANDS (name, COMMISSION) values ('Panasonic', 5);
insert into BRANDS (name, COMMISSION) values ('HP', 6);
insert into BRANDS (name, COMMISSION) values ('LG', 4);
insert into BRANDS (name, COMMISSION) values ('Samsung', 3);
insert into BRANDS (name, COMMISSION) values ('IBM', 7);
insert into BRANDS (name, COMMISSION) values ('Dell', 3);
insert into BRANDS (name, COMMISSION) values ('Lenovo', 5);
insert into BRANDS (name, COMMISSION) values ('Xiaomi', 2);
insert into BRANDS (name, COMMISSION) values ('Sharp', 6);

COMMIT;