--liquibase formatted sql
--changeset pkrawczyk:insert_period_events failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback delete from period_events where 1=1 ; commit;

INSERT INTO PERIOD_EVENTS (NAME, START_DATE, END_DATE, COMMISSION) VALUES ('Black week',to_date('01-11-2022', 'DD/MM/YYYY'),to_date('30-11-2022', 'DD/MM/YYYY'),5);
INSERT INTO PERIOD_EVENTS (NAME, START_DATE, END_DATE, COMMISSION) VALUES ('Christmas sale',to_date('01-12-2022', 'DD/MM/YYYY'),to_date('31-12-2022', 'DD/MM/YYYY'),4);
INSERT INTO PERIOD_EVENTS (NAME, START_DATE, END_DATE, COMMISSION) VALUES ('Back to school',to_date('01-09-2022', 'DD/MM/YYYY'),to_date('25-09-2022', 'DD/MM/YYYY'),2);
INSERT INTO PERIOD_EVENTS (NAME, START_DATE, END_DATE, COMMISSION) VALUES ('Summer sale',to_date('01-06-2022', 'DD/MM/YYYY'),to_date('30-06-2022', 'DD/MM/YYYY'),1);
INSERT INTO PERIOD_EVENTS (NAME, START_DATE, END_DATE, COMMISSION) VALUES ('Wietrzenie magazynów',to_date('01-01-2022', 'DD/MM/YYYY'),to_date('31-01-2022', 'DD/MM/YYYY'),2);

Commit;