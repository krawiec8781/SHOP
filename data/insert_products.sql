--liquibase formatted sql
--changeset pkrawczyk:insert_products failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback delete from products where 1=1 ; commit;


insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SAMSUNG'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'CVV', 3500.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'BOP', 4200.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'LG'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'RXC', 3999.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'TV'),'JDD', 4199.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SAMSUNG'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'WE3', 3500.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SONY'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'GD4', 5000.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'DRUKARKA'),'XC2', 7299.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SONY'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'QW5', 4599.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'W34', 1200.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'LG'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'C56', 999.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'T58', 2599.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SONY'),(select id from PRODUCTS_TYPES where upper(name) = 'TV'),'P87', 877.59);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'TV'),'WERT', 199.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SAMSUNG'),(select id from PRODUCTS_TYPES where upper(name) = 'DRUKARKA'),'QGFR', 129.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'AXCD', 399.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'HP'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'TREX', 499.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SAMSUNG'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'Q123', 199.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'TV'),'R567', 239.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SAMSUNG'),(select id from PRODUCTS_TYPES where upper(name) = 'TELEFON'),'P567', 399.50);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'J678', 899.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'LG'),(select id from PRODUCTS_TYPES where upper(name) = 'TV'),'DF56', 899.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'DRUKARKA'),'PE87', 1999.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'SONY'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'WE45', 3599.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'DRUKARKA'),'WP76', 2599.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'XDW3', 999.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'HP'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'HTY8', 2399.00);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'HP'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'WCT6', 3199.99);
insert into products (BRAND_ID,PRODUCT_TYPE_ID,MODEL_NAME, PRICE) VALUES ((select id from brands where upper(name) = 'APPLE'),(select id from PRODUCTS_TYPES where upper(name) = 'LAPTOP'),'ZXD5', 1299.99);
commit;