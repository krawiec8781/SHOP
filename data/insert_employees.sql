--liquibase formatted sql
--changeset pkrawczyk:insert_employees failOnError:true stripComments:false splitStatements:true rollbackSplitStatements:true runOnChange:false
--rollback delete from employees where 1=1 ; commit;


INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Jan', 'Kowalski', 'Sprzedawca');
INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Andrzej', 'Nowak', 'Sprzedawca');
INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Krzysztof', 'Jarzyna', 'Kierownik sklepu');
INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Alicja', 'Podolska', 'Kierownik zmiany');
INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Paulina', 'Szewczyk', 'Doradca klienta');
INSERT INTO EMPLOYEES (NAME, SURNAME, POSITION) VALUES('Pawel', 'Janik', 'Sprzedwaca');

Commit;

