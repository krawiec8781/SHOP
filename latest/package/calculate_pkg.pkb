CREATE OR REPLACE PACKAGE BODY calculate_pkg
AS

    procedure p_commission
    as
        v_product_price           NUMBER;
        v_product_type_commission NUMBER;
        v_brand_commission        NUMBER;
        v_period_event_commission NUMBER;
        v_total_commission        NUMBER;
    BEGIN

        FOR c IN (SELECT * FROM sales WHERE "Calculated" = 'N')
            LOOP
                --retrieve product price
                SELECT price
                INTO v_product_price
                FROM products
                WHERE id = c.product_id;

                -- retrieve product type commission
                SELECT commission
                INTO v_product_type_commission
                FROM products_types
                WHERE id = (SELECT product_type_id FROM products WHERE id = c.product_id);

                -- retrieve brand commission
                SELECT commission
                INTO v_brand_commission
                FROM brands
                WHERE id = (SELECT brand_id FROM products WHERE id = c.product_id);

                BEGIN
                    -- retrieve period event commission
                    SELECT commission
                    INTO v_period_event_commission
                    FROM period_events
                    WHERE id = c.peroid_events_id;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_period_event_commission := 0;
                END;
                -- calculate total commission
                v_total_commission := v_product_price *
                                      (v_product_type_commission + v_brand_commission + v_period_event_commission) /
                                      100;

                -- insert commission into commissions table
                INSERT INTO commissions (employee_id, month, year, total_commission)
                VALUES (c.employee_id, TO_CHAR(c.sale_date, 'MM'), TO_CHAR(c.sale_date, 'YYYY'), v_total_commission);

                -- update Calculated flag
                UPDATE Sales
                SET "Calculated" = 'T'
                WHERE id = c.id;

            END LOOP;
        COMMIT;
    END;
end;
/

